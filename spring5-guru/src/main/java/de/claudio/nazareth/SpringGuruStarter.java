package de.claudio.nazareth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGuruStarter {
  public static void main(String[] args) {
    SpringApplication.run(SpringGuruStarter.class, args);
  }
}
