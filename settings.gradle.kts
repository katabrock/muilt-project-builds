rootProject.name = "multi-project-builds"
include("core",
        "kotlin-module",
        "junit5-testing",
        "security",
        "redis-cache",
        "spring5-guru",
        "spring-boot-admin")
