plugins {
    id("org.springframework.boot") version "2.1.6.RELEASE" apply false
    id("com.diffplug.gradle.spotless") version "3.23.1"
}

allprojects {
    repositories {
        jcenter()
        mavenCentral()
    }

}

subprojects {
    group = "de.claudio.nazareth"
    version = "0.0.1-SNAPSHOT"
    apply(plugin = "io.spring.dependency-management")
    apply(plugin = "com.diffplug.gradle.spotless")
    spotless {
        java {
            googleJavaFormat()
        }
        kotlinGradle {
            ktlint()
        }
    }
}

configure(subprojects.filter { it.name == "core" || it.name == "security"
        || it.name == "redis-cache" || it.name == "spring5-guru" || it.name == "spring-boot-admin"}) {
    
   apply(plugin = "org.springframework.boot")
}
