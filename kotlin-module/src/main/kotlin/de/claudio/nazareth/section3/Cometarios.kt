package de.claudio.nazareth.section3

/**
 * So para dizer que o kotlin suporta kdoc
 * @param nao tem nda aqui
 */
fun main() {
    //One line comment
    println("Opa")

    /*
     More than one line comment
     */
    println("Opa denovo")

    /*
     * More than one line with style
     */
    println("Hehehehe")

}