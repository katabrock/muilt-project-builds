plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web") {
        exclude(module = "spring-boot-starter-tomcat")
    }
    implementation("org.springframework.boot:spring-boot-starter-undertow")
    implementation("org.springframework.boot:spring-boot-starter-data-redis")
    implementation("org.projectlombok:lombok:1.18.8")
    implementation("org.springframework.boot:spring-boot-starter-cache")
    annotationProcessor("org.projectlombok:lombok")
    runtime("com.h2database:h2:1.4.199")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}