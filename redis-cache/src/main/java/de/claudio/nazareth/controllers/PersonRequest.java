package de.claudio.nazareth.controllers;

import de.claudio.nazareth.repositories.model.Address;
import lombok.Data;

@Data
class PersonRequest {
  String firstName;
  String lastName;
  private Address address;
}
