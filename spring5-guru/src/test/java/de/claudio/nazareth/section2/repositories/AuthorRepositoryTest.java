package de.claudio.nazareth.section2.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import de.claudio.nazareth.section2.entities.Author;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@DisplayName("The author repository")
class AuthorRepositoryTest {

  @Autowired private AuthorRepository authorRepository;
  private Author author;

  @BeforeEach
  void setUp() {
    author = Author.builder().firstName("Claudio").lastName("Nazareth").build();
  }

  @Test
  @DisplayName("-> should save the author in the data base")
  void shouldSaveTheAuthorInThedataBase() {
    Author savedAuthor = authorRepository.save(author);
    Optional<Author> optionalAuthor = authorRepository.findById(savedAuthor.getId());
    assertThat(optionalAuthor.isPresent()).isTrue();
    Author author = optionalAuthor.get();
    assertThat(author.getId()).isNotNull();
    assertThat(author).extracting("firstName", "lastName").contains("Claudio", "Nazareth");
  }
}
