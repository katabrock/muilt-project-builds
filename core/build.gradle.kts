plugins {
    jacoco
    java
    id("org.sonarqube") version "2.7.1"
}

dependencies {
//    compile(project(":kotlin-module"))
    implementation("org.springframework.boot:spring-boot-starter-webflux") {
//        exclude(module = "spring-boot-starter-logging")
//        exclude(module = "logback-classic")
    }
//    implementation("org.springframework.boot:spring-boot-starter-test"){
//        exclude(module = "spring-boot-starter-logging")
// //        exclude(module = "logback-classic")
//    }

    testImplementation("org.junit.jupiter:junit-jupiter:5.4.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}
jacoco {
    toolVersion = "0.8.4"
    reportsDir = file("$buildDir/customJacocoReportDir")
}

tasks {
    jacocoTestCoverageVerification {
        violationRules {
            rule {
                limit {
//                    minimum = "0.2".toBigDecimal()
                }
            }

            rule {
                enabled = true
                element = "CLASS"
                includes = listOf("org.gradle.*")

                limit {
                    counter = "LINE"
                    value = "TOTALCOUNT"
                    maximum = "0.9".toBigDecimal()
                }
            }
        }
    }
    check {
        dependsOn(jacocoTestCoverageVerification)
    }
}

tasks.test {
    useJUnitPlatform()
}

sonarqube {
    properties {
        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.login", "b0e5477790ad2e42d8307b190302c5cbe1f5134e")
        property("sonar.projectKey", "Claudio-Nazareth_muilt-project-builds")
        property("sonar.organization", "katabrock-bitbucket")
    }
}
