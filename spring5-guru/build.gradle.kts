import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension

plugins {
    java
}

dependencies {
    // Boot
    implementation("org.springframework.boot:spring-boot-starter-web") {
        exclude(module = "spring-boot-starter-tomcat")
    }
    implementation("org.springframework.boot:spring-boot-starter-undertow")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    // **************************************************************\\
    implementation("org.projectlombok:lombok:1.18.8")
    implementation("de.codecentric:spring-boot-admin-starter-client:2.1.5")
//    implementation("org.springframework.boot:spring-boot-starter-cache")

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.liquibase:liquibase-core:3.6.3")
    runtime("com.h2database:h2:1.4.199")
    // *********************************** TEST *******************************************\\
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "junit")
        exclude(module = "org.hamcrest")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.4.2")
//    testImplementation("org.junit.platform:junit-platform-launcher:1.4.2")
    testImplementation("org.junit.platform:junit-platform-commons:1.4.2")
    annotationProcessor("org.projectlombok:lombok:1.18.8")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

configure<DependencyManagementExtension> {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:Greenwich.RELEASE")
    }
}

tasks.test {
    useJUnitPlatform()
}