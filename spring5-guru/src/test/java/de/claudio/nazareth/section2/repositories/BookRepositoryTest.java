package de.claudio.nazareth.section2.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import de.claudio.nazareth.section2.entities.Book;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@DisplayName("The BookRepository")
class BookRepositoryTest {

  private Book book;
  @Autowired private BookRepository repository;

  @BeforeEach
  void setUp() {
    book = Book.builder().isbn("123456").title("Java code").publisher("BookNerd").build();
  }

  @Test
  @DisplayName("-> should save the book in the data base")
  void shouldSaveTheBookInTheDataBase() {
    Book savedBook = repository.save(book);
    Optional<Book> optionalBook = repository.findById(savedBook.getId());
    assertThat(optionalBook.isPresent()).isTrue();
    Book book = optionalBook.get();
    assertThat(book.getId()).isNotNull();
    assertThat(book)
        .extracting("isbn", "title", "publisher")
        .contains("123456", "Java code", "BookNerd");
  }
}
