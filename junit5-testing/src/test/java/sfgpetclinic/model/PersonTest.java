package sfgpetclinic.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class PersonTest {

  @Test
  @Disabled(value = "Test disable")
  void groupedAssertions() {
    // GIVEN
    Person person = new Person(1L, "Joe", "Buck");
    // THEN
    assertAll(
        "Test Props Set",
        () -> assertEquals(person.getFirstName(), "Joe"),
        () -> assertEquals(person.getLastName(), "Buck"));
  }
}
