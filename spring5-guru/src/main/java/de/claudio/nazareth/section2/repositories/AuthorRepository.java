package de.claudio.nazareth.section2.repositories;

import de.claudio.nazareth.section2.entities.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {}
