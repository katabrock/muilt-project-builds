package de.claudio.nazareth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaApplicationStarter {
  public static void main(String[] args) {
    if (true) {
      System.out.println();
    }
    SpringApplication.run(JavaApplicationStarter.class, args);
  }
}
