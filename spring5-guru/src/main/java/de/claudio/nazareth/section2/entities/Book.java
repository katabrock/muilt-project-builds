package de.claudio.nazareth.section2.entities;

import java.util.Set;
import javax.persistence.*;
import lombok.*;

@Value
@Entity
@Builder
@Table(name = "BOOK")
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "TITLE", nullable = false, length = 50)
  private String title;

  @Column(name = "ISBN", nullable = false, length = 30)
  private String isbn;

  @Column(name = "publisher", nullable = false, length = 50)
  private String publisher;

  @ManyToMany
  @JoinTable(
      name = "author_book",
      joinColumns = @JoinColumn(name = "book_id"),
      inverseJoinColumns = @JoinColumn(name = "author_id"))
  private Set<Author> authors;
}
