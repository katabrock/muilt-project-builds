package de.claudio.nazareth.section2.entities;

import java.util.Set;
import javax.persistence.*;
import lombok.*;
import org.springframework.lang.NonNull;

@Value
@Entity
@Builder
@Table(name = "AUTHOR")
public class Author {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NonNull
  @Column(name = "FIRST_NAME", nullable = false, length = 25)
  private String firstName;

  @NonNull
  @Column(name = "LAST_NAME", nullable = false, length = 50)
  private String lastName;

  @ManyToMany(mappedBy = "authors")
  private Set<Book> books;
}
