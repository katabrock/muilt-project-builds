package sfgpetclinic.repositories;

import java.util.List;
import sfgpetclinic.model.Owner;

public interface OwnerRepository extends CrudRepository<Owner, Long> {

  Owner findByLastName(String lastName);

  List<Owner> findAllByLastNameLike(String lastName);
}
