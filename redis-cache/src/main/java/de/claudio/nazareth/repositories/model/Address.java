package de.claudio.nazareth.repositories.model;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address implements Serializable {
  private String street;
  private String number;
  private String state;
  private String country;
  private String zip;
}
