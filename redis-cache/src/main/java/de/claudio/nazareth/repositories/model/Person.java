package de.claudio.nazareth.repositories.model;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Data
@Builder
@RedisHash("people")
public class Person implements Serializable {

  @Id private String id;
  @Indexed private String firstName;
  private String lastName;
  private Address address;
}
