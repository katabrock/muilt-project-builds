package sfgpetclinic.controllers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("The Index Controller")
class IndexControllerTest {

  IndexController controller;

  @BeforeEach
  void setUp() {
    controller = new IndexController();
  }

  @Test
  @DisplayName("should return index")
  void index() {
    assertEquals("index", controller.index());
    assertEquals("index", controller.index(), "Wrong View returned");
  }

  @Test
  void oupsHandler() {
    assertEquals(
        "notimplemented",
        controller.oupsHandler(),
        () -> "This is some expensive message build form e test");
  }
}
