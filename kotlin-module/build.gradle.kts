import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `kotlin-dsl`
}
dependencies {
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}