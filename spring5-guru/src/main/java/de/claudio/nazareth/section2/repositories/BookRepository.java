package de.claudio.nazareth.section2.repositories;

import de.claudio.nazareth.section2.entities.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {}
