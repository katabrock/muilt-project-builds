package de.claudio.nazareth.controllers;

import de.claudio.nazareth.repositories.PersonRepository;
import de.claudio.nazareth.repositories.model.Person;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/person")
@AllArgsConstructor
public class PersonController {

  private PersonRepository personRepository;

  @PostMapping
  public ResponseEntity createPerson(@RequestBody PersonRequest personRequest) {
    personRepository.save(
        Person.builder()
            .firstName(personRequest.firstName)
            .lastName(personRequest.lastName)
            .address(personRequest.getAddress())
            .build());
    return ResponseEntity.ok(HttpStatus.OK);
  }

  @GetMapping
  @Cacheable("people")
  public Iterable<Person> getAll() {
    log.info("request entrando");
    return personRepository.findAll();
  }
}
