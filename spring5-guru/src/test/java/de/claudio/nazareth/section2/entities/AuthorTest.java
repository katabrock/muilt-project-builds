package de.claudio.nazareth.section2.entities;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("The Author")
class AuthorTest {

  @Test
  @DisplayName("-> should have a builder")
  void shouldHaveBuilder() {
    Author author = Author.builder().firstName("Claudio").lastName("Nazareth").build();
    assertThat(author).extracting("firstName", "lastName").contains("Claudio", "Nazareth");
  }

  @Test
  @DisplayName("-> should have a name")
  void shouldHaveName() {
    Throwable thrown = catchThrowable(() -> Author.builder().lastName("Nazareth").build());
    assertThat(thrown).hasMessageContaining("firstName is marked non-null but is null");
  }

  @Test
  @DisplayName("-> shouldHaveLastName")
  void shouldHaveLastName() {
    Throwable thrown = catchThrowable(() -> Author.builder().firstName("Claudio").build());
    assertThat(thrown).hasMessageContaining("lastName is marked non-null but is null");
  }
}
