package de.claudio.nazareth;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAdminServer
@SpringBootApplication
public class SpringBootAdminStarter {
  public static void main(String[] args) {
    SpringApplication.run(SpringBootAdminStarter.class, args);
  }
}
