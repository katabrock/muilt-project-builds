package de.claudio.nazareth.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Slf4j
@EnableCaching
@Configuration
@EnableRedisRepositories(basePackages = {"de.claudio.nazareth.redis.repositories"})
public class RedisConfiguration {

  @Bean
  public LettuceConnectionFactory redisConnectionFactory() {
    log.info("Starting Lettuce Connection Factory");
    return new LettuceConnectionFactory(new RedisStandaloneConfiguration());
  }
}
